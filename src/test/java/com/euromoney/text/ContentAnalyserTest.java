package com.euromoney.text;

import org.junit.Test;

import static com.euromoney.ConsoleContent.Program.DEFAULT_NEGATIVE_WORDS;
import static com.euromoney.ConsoleContent.Program.ECHO_FORMATTER;
import static com.euromoney.ConsoleContent.Program.HASH_FORMATTER;
import static com.euromoney.text.StringUtilsExtension.asSet;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;


public class ContentAnalyserTest {


    @Test
    public void should_return_no_content_with_zero_negative_word_count_given_content_is_blank(){
        //Given
        ContentAnalyser contentAnalyser = new ContentAnalyser(asSet(DEFAULT_NEGATIVE_WORDS), ECHO_FORMATTER);
        //When
        final Result nullContentResult = contentAnalyser.analyse(null);
        //Then
        assertThat(nullContentResult, is(new Result("", 0)));
        //When
        final Result blankContentResult = contentAnalyser.analyse("");
        //Then
        assertThat(blankContentResult, is(new Result("", 0)));
    }

    @Test
    public void should_return_content_with_negative_word_count_for_given_content(){
        //Given
        ContentAnalyser contentAnalyser = new ContentAnalyser(asSet(DEFAULT_NEGATIVE_WORDS), ECHO_FORMATTER);
        String content = "The weather in Manchester in winter is bad. It rains all the time - it must be horrible for people visiting.";
        //When
        final Result analysedResult = contentAnalyser.analyse(content);
        //Then
        assertThat(analysedResult, is(new Result(content, 2)));
    }


    @Test
    public void should_increment_negative_word_count_given_negative_word_is_repeated(){
        //Given
        ContentAnalyser contentAnalyser = new ContentAnalyser(asSet(DEFAULT_NEGATIVE_WORDS), ECHO_FORMATTER);
        String content = "The weather in Manchester in winter is BAD. It rains all the time - it must be BAD for people visiting.";
        //When
        final Result analysedResult = contentAnalyser.analyse(content);
        //Then
        assertThat(analysedResult, is(new Result(content, 2)));
    }

    @Test
    public void should_format_the_negative_words(){
        //Given
        ContentAnalyser contentAnalyser = new ContentAnalyser(asSet(DEFAULT_NEGATIVE_WORDS), HASH_FORMATTER);
        String content = "The weather in Manchester in winter is bad. It rains all the time - it must be horrible for people visiting.";
        String formattedContent = "The weather in Manchester in winter is b#d. It rains all the time - it must be h######e for people visiting.";
        //When
        final Result analysedResult = contentAnalyser.analyse(content);
        //Then
        assertThat(analysedResult, is(new Result(formattedContent, 2)));
    }

}