package com.euromoney.text;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static com.euromoney.text.StringUtilsExtension.asSet;
import static com.euromoney.text.StringUtilsExtension.csvToSet;
import static com.euromoney.text.StringUtilsExtension.hashMiddleString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;


public class StringUtilsExtensionTest {


    @Test
    public void should_convert_array_of_strings_to_set(){
        //Given
        final String[] arrayOfStrings = {"blah", "blah", "blaah"};
        Set<String> expectedSet = new HashSet<String>(){{
            add("blah");
            add("blaah");
        }};
        //When
        final Set<String> setOfStrings = asSet(arrayOfStrings);
        //Then
        assertThat(setOfStrings, is(notNullValue()));
        assertThat(setOfStrings.size(), is(2));
        assertThat(setOfStrings, is(expectedSet));
    }


    @Test
    public void should_convert_csv_to_set(){
        //Given
        String csv = "blah, blaah, blah";
        Set<String> expectedSet = new HashSet<String>(){{
            add("blah");
            add("blaah");
        }};
        //When
        final Set<String> setOfStrings = csvToSet(csv);
        //Then
        assertThat(setOfStrings, is(notNullValue()));
        assertThat(setOfStrings.size(), is(2));
        assertThat(setOfStrings, is(expectedSet));
    }


    @Test
    public void should_hash_middle_part_of_string(){
        assertThat(hashMiddleString(null), is(""));
        assertThat(hashMiddleString(""), is(""));
        assertThat(hashMiddleString("Blaah"), is("B###h"));
    }

}