package com.euromoney.text;

import org.apache.commons.lang3.StringUtils;

import java.util.Set;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toSet;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.split;

public class StringUtilsExtension {

    public static Set<String> asSet(String[] strings) {
        return Stream.of(strings).collect(toSet());
    }

    public static Set<String> csvToSet(String csv) {
        return Stream.of(split(csv, ","))
                .map(StringUtils::trim)
                .collect(toSet());
    }

    public static String hashMiddleString(String string){
        if(isBlank(string)) return EMPTY;

        return new StringBuilder().append(string.charAt(0))
                .append(hashes(string.length() - 2))
                .append(string.charAt(string.length() -1))
                .toString();
    }

    private static String hashes(int i) {
        return IntStream.range(0, i)
                .mapToObj(n -> "#")
                .collect(joining());
    }
}
