package com.euromoney.text;

import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.*;

public class ContentAnalyser {

    private final Set<String> negativeWords;
    private final Function<String,String> negativeWordFormatter;

    public ContentAnalyser(Set<String> negativeWords, Function<String, String> negativeWordFormatter) {
        this.negativeWords = negativeWords;
        this.negativeWordFormatter = negativeWordFormatter;
    }

    public Result analyse(String content){
        if(isBlank(content)) return new Result(EMPTY, 0);

        List<String> negativeWordsInContent = extractNegativeWordsInContent(content);
        return new Result(formatText(content, negativeWordsInContent), negativeWordsInContent.size());
    }

    private List<String> extractNegativeWordsInContent(String content) {
        return Stream.of(content.split("[\\p{Punct}\\s]+"))
                .filter(token -> negativeWords.contains(lowerCase(token)))
                .collect(toList());
    }

    private String formatText(String content, List<String> negativeWordsInContent){
        final String[] arrayOfNegativeWordsInContent = negativeWordsInContent.toArray(new String[negativeWordsInContent.size()]);
        final String[] formattedNegativeWords = negativeWordsInContent.stream()
                .map(negativeWordFormatter)
                .collect(toList())
                .toArray(new String[negativeWordsInContent.size()]);

        return replaceEach(content, arrayOfNegativeWordsInContent, formattedNegativeWords);
    }

}
