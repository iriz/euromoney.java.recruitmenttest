package com.euromoney.text;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import static org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString;

public class Result {

    private final String formattedContent;
    private final int negativeWordCount;

    public Result(String formattedContent, int negativeWordCount) {
        this.formattedContent = formattedContent;
        this.negativeWordCount = negativeWordCount;
    }

    public String getFormattedContent() {
        return formattedContent;
    }

    public int getNegativeWordCount() {
        return negativeWordCount;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Result that = (Result) o;

        return new EqualsBuilder().append(formattedContent, that.formattedContent)
                .append(negativeWordCount, that.negativeWordCount)
                .isEquals();
    }

    @Override
    public String toString() {
        return reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(47, 71).append(formattedContent)
                .append(negativeWordCount)
                .toHashCode();
    }
}
