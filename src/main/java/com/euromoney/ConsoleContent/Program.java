package com.euromoney.ConsoleContent;

import com.euromoney.text.ContentAnalyser;
import com.euromoney.text.Result;
import com.euromoney.text.StringUtilsExtension;

import java.io.IOException;
import java.util.Scanner;
import java.util.Set;
import java.util.function.Function;

import static com.euromoney.text.StringUtilsExtension.asSet;
import static com.euromoney.text.StringUtilsExtension.csvToSet;
import static org.apache.commons.lang3.StringUtils.*;

public class Program {

	public static final String[] DEFAULT_NEGATIVE_WORDS = {"swine","bad","nasty","horrible"};
	public static final Function<String, String> ECHO_FORMATTER = s -> s;
	public static final Function<String, String> HASH_FORMATTER = StringUtilsExtension::hashMiddleString;

	/**
	 * Initialises the application in the
	 * console.
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {

		Scanner scan = new Scanner(System.in);
		System.out.print(String.format("Please provide comma separate list (blah, blah, blah) of negative words or press ENTER to use default [%s]:", join(DEFAULT_NEGATIVE_WORDS, ",")));
		String input = scan.nextLine();
		Set<String> negativeWords = isBlank(input)? asSet(DEFAULT_NEGATIVE_WORDS) : csvToSet(lowerCase(input)) ;
		System.out.print("Do you want to hash the negative words? Press ENTER to continue or `N` to disable it.");
		input = scan.nextLine();
		Function<String, String> formatter = "N".equalsIgnoreCase(trim(input))? ECHO_FORMATTER : HASH_FORMATTER;

		System.out.println("Please provide the text to analyse:");
		String content= scan.nextLine();

        final ContentAnalyser contentAnalyser = new ContentAnalyser(negativeWords, formatter);
        Result result = contentAnalyser.analyse(content);

		// TODO Implement your logic to analyze the text.
		//  As first approximation the content can be considered as a sequence
		//   of words where each word is a sequence of letters of the basic alphabet [a-z].
		//  If usefull, you are allowed to use any java external libraries.
		
		System.out.println("\nScanned the text sequence: " + result.getFormattedContent() + "\n");
		System.out.println("\nTotal number of banned words: " + result.getNegativeWordCount() + "\n");
		System.out.println("\nPress ENTER to exit!\n");
		System.in.read();
		System.out.println("\nExiting Application!\n");
	}


}
